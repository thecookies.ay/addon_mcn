import bpy
import random


class MCN_OT_AY(bpy.types.Operator):
    bl_idname = "random.city"
    bl_label = "Random City"
    bl_options = {'REGISTER','UNDO'}
    
    def execute(self,context):
        
        for position_x in range (100):
            bpy.ops.mesh.primitive_cube_add(location=(0, 0, 0))
            bpy.context.active_object.location.x = position_x
            bpy.context.active_object.location.y = position_x
    
            bpy.context.active_object.scale.z = random.uniform (1,10)
            bpy.context.active_object.location.x = random.uniform (1,100)
            bpy.context.active_object.location.y = random.uniform (1,100)
            
            return {'FINISHED'}

class MCN_PT_AY(bpy.types.Panel):
    """"""
    bl_label = "AY"
    bl_idname = "MCN_PT_AY"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"
    bl_parent_id = "MCN_PT_Main_Panel"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        layout.label(text="Faire une ville randomisée")
        layout.operator("random.city", icon="QUESTION")
        
        

def register():
    bpy.utils.register_class(MCN_PT_AY)
    bpy.utils.register_class(MCN_OT_AY)


def unregister():
    bpy.utils.unregister_class(MCN_PT_AY)
    bpy.utils.unregister_class(MCN_OT_AY)
    
#if __name__ == "__main__":
#    register()